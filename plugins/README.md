# Cordova Plugins

All Cordova plugins added to the application will be here. Do not manage these files/directories by hand - use cordova cli instead.

To add a new plugin, execute this command anywhere in the project's directories:

```bash
cordova plugin add <plugin-name>

```

Example:

```bash
cordova plugin add cordova-plugin-network-information

```